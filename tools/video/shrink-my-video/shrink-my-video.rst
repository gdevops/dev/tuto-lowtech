.. index::
   pair: Tool; Shrink my video
   pair: Video; Shrink my video
   ! Shrink my video


.. _shrink_my_video:

===================================
Shrink my video !
===================================

- https://shrink-my-video.onrender.com/shrink-my-video/
- https://github.com/acailly/shrink-my-video/
- https://github.com/acailly
- https://acailly.github.io/
- https://theshiftproject.org/en/guide-reduce-weight-video-5-minutes/

.. figure::  images/shrink.png
   :align: center



Why this tool ?
======================

TL;DR; to help reduce the environmental footprint of the digital world

This tool aims to humbly participate in stopping the race for ever more
quality and bandwidth (8K resolution, 5G network, etc.) by offering a
simple way to radically reduce the size of a video.

The digital world’s contribution to the footprint of humanity is currently
around 4% (see this report from GreenIT.fr and this another report from
The Shift Project).

This is more than the aviation, and it is predicted to grow in the future.

Online video does NOT represent the main impact of the digital world,
**manufacturing is a much bigger deal**.

But reducing the size of a video can help keep our old devices that have
limited bandwidth and limited computing power, instead of buying new ones.



What does it do ?
======================

Apply settings from The Shift Project (720p, CRF 22, 160 kbits/s)
------------------------------------------------------------------------

- https://theshiftproject.org/en/guide-reduce-weight-video-5-minutes/


The Shift Project has published a guide for reducing the size of a video
while maintaining a good quality

You can download it `here <https://theshiftproject.org/en/guide-reduce-weight-video-5-minutes/>`_

Basically, it reduces the video resolution to 720p and the audio bitrate
to 160 kbits/s. The Constant Rate Factor (CRF) is set to 22.

Lower video resolution (480p)
--------------------------------

It reduces the video resolution to 480p.
Don't watch it on your 4K TV, otherwise it is OK.


Lower video quality (CRF 28)
---------------------------------

It reduces the Constant Rate Factor (CRF) to 28.
You won't see tiny details but the essential is there.

Lower audio bitrate (56 kbits/s)
-----------------------------------

It reduces the audio bitrate to 56 kbits/s.
If the audio is mainly speeches, it should remain audible.

One image every 5 seconds (!)
---------------------------------

For some videos, like conferences, there is not much action and you can
easily understand what happens even with one image every 5 seconds

Black and white
-------------------

Aesthetic and lightweight



How it works
===================

- https://theshiftproject.org/en/guide-reduce-weight-video-5-minutes/
- https://ffmpegwasm.github.io/
- https://ffmpeg.org/
- https://ditherit.com/

Let's get into the technical details

This tool uses `FFMPEG.WASM (https://ffmpegwasm.github.io/) <https://ffmpegwasm.github.io/>`_ to edit the video directly in the browser !

You can download `FFMPEG (https://ffmpeg.org/) <https://ffmpeg.org/>`_ to try these settings directly on your computer

Apply settings from The Shift Project (720p, CRF 22, 160 kbits/s)
------------------------------------------------------------------------

::

    ffmpeg -i input.mp4 -s hd720 -c:v libx264 -crf 22 -vf "scale=iw*sar:ih,setsar=1" -c:a aac -b:a 160k output.mp4


Lower video resolution (480p)
--------------------------------

::

    ffmpeg -i input.mp4 -s hd480 -c:v libx264 -crf 22 -vf "scale=iw*sar:ih,setsar=1" output.mp4

Lower video quality (CRF 28)
------------------------------

::

    ffmpeg -i input.mp4 -c:v libx264 -crf 28 -vf "scale=iw*sar:ih,setsar=1" output.mp4

Lower audio bitrate (56 kbits/s)
----------------------------------

::

    ffmpeg -i input.mp4 -c:a aac -b:a 56k output.mp4

One image every 5 seconds
----------------------------

::

    ffmpeg -i input.mp4 -vf "fps=fps=1/5" output.mp4

Black and white
--------------------

::

    ffmpeg -i input.mp4 -vf "hue=s=0" output.mp4

This whole approach is inspired by image dithering (see `here (https://ditherit.com/) <https://ditherit.com/>`_ for an example),
which removes a lot of data in the image while preserving its essential meaning




