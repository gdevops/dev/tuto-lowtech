.. index::
   pair: Tools; Video

.. _video_tools:

===================================
Video Tools
===================================

.. toctree::
   :maxdepth: 4

   shrink-my-video/shrink-my-video
