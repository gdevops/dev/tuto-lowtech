.. index::
   ! Low Tech


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-lowtech/rss.xml>`_

.. _tuto_lowtech:

===================================
**Low tech**
===================================

.. toctree::
   :maxdepth: 4

   news/news
   tools/tools
