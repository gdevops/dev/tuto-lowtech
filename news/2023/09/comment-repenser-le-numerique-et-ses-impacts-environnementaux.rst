.. index::
   pair: Projet ; ALDIWO (Anti-Limits in the Digital World)
   ! ALDIWO (Anti-Limits in the Digital World)

.. _lowtech_2023_09_20:

=================================================================================================================================================================================================================================================
2023-09-20 **Journée « Low-tech, maintenance, limites :comment repenser le numérique et ses impacts environnementaux »** organiséee par  Clément Marquet (CSI, Mines Paris – PSL, i3) et Florence Maraninchi (Verimag, Grenoble INP – UGA)
=================================================================================================================================================================================================================================================

- https://www.csi.minesparis.psl.eu/agenda/journee-low-tech-maintenance-limites-comment-repenser-le-numerique-et-ses-impacts-environnementaux/

Organisation
===================

Événement organisé dans le cadre du projet ALDIWO (Anti-Limits in the Digital World)
(synthèse du projet ci-après).

Ce projet a obtenu le soutien financier du CNRS à travers les programmes
interdisciplinaires de la MIT.

En français, pas de visioconférence.

Organisateur.rice : Clément Marquet (CSI, Mines Paris – PSL, i3) et
Florence Maraninchi (Verimag, Grenoble INP – UGA)

Descriptif de la journée
=============================

Les sciences du numérique et le développement technologique sont dominées
par un ensemble de mots d’ordre incompatibles avec les enjeux de sobriété
auxquels nous faisons face : course à la puissance de calcul et à la
vitesse des échanges, accélération de l’obsolescence matérielle et
logicielle, invisibilisation de la matérialité du dispositif technique
(voir synthèse du projet ALDIWO ci-après).

Lors de cette journée, nous proposons un échange interdisciplinaire pour
explorer d’autres pistes de recherche vers un système technique plus s
outenable – et éventuellement plus dénumérisé.

Pour cela, la journée s’organisera en deux temps : un premier temps sera
consacré à des présentations de sociologie et des sciences et des
techniques et de sciences informatique portant sur des modalités alternatives
ou dévalorisées de se rapporter au développement technologique – qui ne
constituent pas pour autant des solutions prêtes à l’emploi !

Un second temps prendra la forme d’ateliers visant à dessiner des
perspectives de recherche interdisciplinaire pour explorer des futurs
socio-technico-écologiques souhaitables et soutenables, en interrogeant
ce que deviendrait « le numérique » si on lui réintroduisait des limites.

Matin – Présentations (9h30-12h15)
---------------------------------------

- 9h15 – 9h30 : accueil
- 9h30 – 9h40 : Introduction – Clément Marquet (CSI, Mines Paris – PSL, i3)
- 9h45 – 10h30 : Morgan Meyer (CSI, CNRS, i3) Low-techs et objets frontières
- 10h30 – 11h15 : Jérôme Denis (CSI, Mines Paris – PSL, i3) et David Pontille
  (CSI, CNRS, i3) Maintenance et attention à la fragilité

Pause
--------

- 11h30 – 12h15 : Florence Maraninchi (Verimag, Grenoble INP – UGA)
  *Contraintes, limites, optimisation : quelques exemples vus de  l’intérieur
  de l’informatique*


Repas 12h15-14h
====================

Après-midi – Ateliers (14h-17h)
=====================================

Objectif : faire émerger des questions de recherche pluridisciplinaires

Format des ateliers à venir


.. _projet_aldiwo:

Argumentaire du projet ALDIWO (Anti-Limits in the Digital World)
=========================================================================

Les impacts environnementaux du numérique sont étudiés en recherche sous
deux angles principaux :

- (1) les approches dites “Green IT” s’attaquent à l’optimisation de
  consommation électrique des équipements numériques en phase d’usage ;
- (2) les approches dites “Green-by-IT” ont l’ambition de réduire les
  impacts environnementaux de secteurs autres que le numérique, grâce
  au numérique.

Ces directions actuelles ne suffisent pas à couvrir l’ensemble des futurs
envisageables quant à la place du numérique dans les bouleversements
environnementaux actuels et à venir :

- (1) il n’y a pas d’exemple dans l’histoire du numérique, où les
  optimisations n’aient pas été accompagnées d’effets rebond massifs
  qui annulent les gains de ces optimisations ;
- (2) les gains théoriques espérés en proposant une nouvelle génération
  d’équipements numériques ne sont pas toujours au rendez-vous en pratique,
  en partie parce qu’une génération ne remplace pas la précédente, mais
  s’y ajoute, au moins pendant un certain temps ;
- (3) il est très difficile d’évaluer la potentielle réduction des impacts
  environnementaux des autres secteurs qu’on cherche à optimiser grâce
  au numérique.
  C’est un pari risqué de tout miser sur le fait que ces réductions seront
  suffisamment importantes pour autoriser le numérique à ne pas réduire
  ses propres impacts.

Pour que le numérique prenne sa part de la nécessaire réduction des
impacts environnementaux globaux, il faut absolument envisager d’autres
pistes que le green-IT et le green-by-IT.

Il y a tout un pan de recherche potentielle en informatique qui constitue
de la science non faite (*Undone Science* [1]).

Ces sujets pourraient être étudiés depuis l’intérieur de la discipline
informatique, en réexaminant systématiquement les choix de conception
des systèmes anciens ou modernes, en cherchant des points d’optimisation
potentielle encore non explorés, en évaluant la fragilité des infrastructures
numériques vis-à-vis de contraintes drastiques imposées de l’extérieur, etc.

La réflexion cantonnée aux domaines de recherche du numérique nous semble
cependant avoir deux défauts principaux : (a) elle est intrinsèquement
limitée aux aspects pratiques et technologiques de la question, sans bien
savoir par exemple comment tenir compte des usages, des effets rebond,
des effets d’accélération de tous les autres secteurs, etc. ; (b) elle
pâtit, même si c’est parfois inconscient, d’un cadre de réflexion qui
voit les technologies numériques comme un (voire le seul) moyen d’ouvrir
les futurs possibles, et toute idée de limitation comme relevant d’un
pessimisme irréaliste ou d’un manque de confiance condamnable.

Or dans son article “Predictions Without Futures” [2], S. Hong décrit
parfaitement comment l’imaginaire du monde technologique, loin d’ouvrir
les futurs, les referme complètement.

Dans ce projet, nous proposons donc d’explorer la notion de « limite »
dans un cadre pluridisciplinaire, afin de dessiner des pistes pour un
programme de recherche prenant au sérieux le projet d’une sobriété numérique.

[1] S. Frickel, S. Gibbon, J. Howard, J. Kempner, G. Ottinger, and D. J.
Hess, “Undone Science: Charting Social Movement and Civil Society Challenges
to Research Agenda Setting,” Science, Technology, & Human Values, vol. 35,
no. 4, pp. 444–473, Jul. 2010, doi: 10.1177/0162243909345836.

[2] S. Hong, “Predictions Without Futures,” History and Theory,
vol. 61, no. 3, pp. 371–390, 2022, doi: 10.1111/hith.12269.


